using UnityEngine;

public class Weapon_FlameThrower : Weapon
{
    bool isFiring;
    float power;

    public override void Fire()
    {
        isFiring = true;
        power = ShootPower;
    }

    public override void EndFire()
    {
        isFiring = false;
    }

    private void Update()
    {
        if (!isFiring) { return; }
        Ray ray = new Ray(SpawnPoint.position, SpawnPoint.forward);
        RaycastHit hitInfo;
        Debug.DrawRay(SpawnPoint.position, SpawnPoint.forward * power, Color.red);
        if (Physics.Raycast(ray, out hitInfo, power))
        {
            Enemy enemy = hitInfo.transform.GetComponent<Enemy>();
            if (enemy != null)
            {
                //Destroy(enemy.gameObject);
                enemy.TakeDamage(Damage * Time.deltaTime);
            }
        }
        power -= Time.deltaTime * 3f;
    }
}
