using System;
using UnityEngine;

// TODO: Gravity
public class Player : MonoBehaviour
{
    public GUIStyle style;
    public int BonusPoints;
    public float Health = 100f;
    public Moving Moving;
    AimingWithCursor aiming;
    Shooting shooting;
    // GUI variables
    float gap = 10f;
    Vector2 buttonSize = new Vector2(80f, 40f);
    Vector2 labelSize = new Vector2(120f, 40f);
    float difficulty = 0f;

    private void Awake()
    {
        aiming = GetComponent<AimingWithCursor>();
        shooting = GetComponent<Shooting>();
    }

    void Update()
    {
        Moving.OnUpdate();
    }

    public void AddBonusPoints(int bp)
    {
        BonusPoints += bp;
    }

    public void SwitchState(PlayerState state)
    {
        switch (state)
        {
            case PlayerState.Bonus:
                Moving = GetComponent<LRMoving>();
                aiming.enabled = true;
                shooting.enabled = true;
                break;
            case PlayerState.Off:
                Moving = GetComponent<NoMoving>();
                aiming.enabled = false;
                shooting.enabled = false;
                break;
            case PlayerState.Level:
                Moving = GetComponent<FreeMoving>();
                aiming.enabled = true;
                shooting.enabled = true;
                break;
            default:
                break;
        }
        aiming.SetPlane(state);
    }

    public void TakeDamage(int damage)
    {
        Health -= damage;
    }

    void OnGUI()
    {
        //GUILayout.Label(Health.ToString(), style);
        if(GUI.Button(new Rect(new Vector2(0f, 0f), buttonSize), "Press me!"))
        {
            Health = 100f;
        }
        GUI.Button(new Rect(new Vector2(buttonSize.x + gap, 0f), buttonSize), Health.ToString());
        GUI.Button(new Rect(new Vector2(buttonSize.x * 2f + gap * 2f, 0f), buttonSize), BonusPoints.ToString());

        difficulty = GUI.HorizontalSlider(new Rect(new Vector2(0f, buttonSize.y + gap), new Vector2(100f, 30f)), difficulty, 0f, 100f);
        GUI.Label(new Rect(new Vector2(100f + gap, buttonSize.y + gap), buttonSize), difficulty.ToString(), style);

        GUI.Label(new Rect(new Vector2(Screen.width / 2f, 80f), buttonSize), "Mitte", style);
    }
}

public enum PlayerState
{
    Bonus, Off, Level
}
