using UnityEngine;

public class TexToCubeHeight : MonoBehaviour
{
    public Texture2D Tex;
    public float maxOffset = 1f;
    Color[] pixels;
    GameObject[] cubes;

    void Start()
    {
        pixels = Tex.GetPixels();
        cubes = new GameObject[pixels.Length];
        for (int y = 0; y < Tex.height; y++)
        {
            for (int x = 0; x < Tex.width; x++)
            {
                GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
                cube.transform.position = new Vector3(x, y, 0f);
                int pixelIdx = (y * Tex.width) + x;
                cubes[pixelIdx] = cube;
            }
        }

        for (int i = 0; i < cubes.Length; i++)
        {
            cubes[i].GetComponent<Renderer>().material.color = pixels[i];
            float val = pixels[i].grayscale;
            float cubeZ = Mathf.Lerp(0f, maxOffset, val);
            cubes[i].transform.position = new Vector3(
                cubes[i].transform.position.x,
                cubes[i].transform.position.y,
                cubeZ);
            cubes[i].transform.localScale = Vector3.one * val;
        }
    }
}
