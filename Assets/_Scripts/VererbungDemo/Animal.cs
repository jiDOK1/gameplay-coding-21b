using UnityEngine;

public abstract class Animal
{
    // public variable, protected would probably be better
    public Color ToothColor = Color.white;

    // public properties that must be implemented
    public abstract Color SkinColor { get; }
    public abstract Color NoseColor { get; }
    public abstract float NoseLength { get; }
    public abstract float Scale { get; set; }

    // public methods that must be implemented
    public abstract void Sleep();
    public abstract string Talk();
    public abstract GameObject GetEars();
}
