using UnityEngine;

public class MorphingAnimal : MonoBehaviour
{
    [SerializeField] KeyCode morphKey;
    [SerializeField] Transform pivot;
    [SerializeField] Transform nosePivot;
    [SerializeField] Renderer bodyRend;
    [SerializeField] Renderer toothRend;
    [SerializeField] Renderer noseRend;
    Elephant ele;
    Cat cat;
    Elephant dwarfEle;
    GUIStyle myStyle;
    int animalIdx;
    string noise;

    void Start()
    {
        ele = new Elephant();
        cat = new Cat();
        dwarfEle = new DwarfElephant();

        MorphInto(ele);
    }

    void Update()
    {
        if (Input.GetKeyDown(morphKey))
        {
            switch (animalIdx)
            {
                case 0:
                    MorphInto(ele);
                    break;
                case 1:
                    MorphInto(cat);
                    break;
                case 2:
                    MorphInto(dwarfEle);
                    break;
                default:
                    break;
            }
        }
    }

    void MorphInto(Animal animal)
    {
        pivot.localScale = Vector3.one * animal.Scale;
        nosePivot.localScale = new Vector3(1f, 1f, animal.NoseLength);
        bodyRend.material.color = animal.SkinColor;
        toothRend.material.color = animal.ToothColor;
        noseRend.material.color = animal.NoseColor;
        noise = animal.Talk();
        animalIdx++;
        if (animalIdx > 2)
        {
            animalIdx = 0;
        }
    }

    private void OnGUI()
    {
        myStyle = new GUIStyle(GUI.skin.label);
        myStyle.fontSize = 50;
        myStyle.normal.textColor = Color.red;
        GUILayout.Label(noise, myStyle);
    }
}
