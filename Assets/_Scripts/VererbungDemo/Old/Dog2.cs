using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dog2 : Animal2
{
    void Start()
    {
        Talk();
    }

    public override void Talk()
    {
        base.Talk();
        Debug.Log("And I am a dog!");
    }

    public override void Age()
    {
        age += 2;
    }
}
