using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cat2 : Animal2
{
    void Start()
    {
        age = 15;
        Talk();
    }

    public override void Talk()
    {
        Debug.Log("I am a cat");
        base.Talk();
    }

    public override void Age()
    {
        age++;
    }
}
