using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Animal2 : MonoBehaviour
{
    [SerializeField] protected int age;
    [SerializeField] protected string myName;
    [SerializeField] protected string noise;

    public virtual void Talk()
    {
        Debug.Log("Hi, my name is " + myName);
        Debug.Log(noise);
    }

    public abstract void Age();

    public void Bla()
    {
    }
}
