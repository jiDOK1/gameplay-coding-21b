using UnityEngine;

public class Elephant : Animal
{
    Color skinColor = Color.gray;
    public override Color SkinColor
    {
        get { return skinColor; }
    }

    Color noseColor = new Color(0.2f, 0.2f, 0.2f, 1.0f);
    public override Color NoseColor
    {
        get { return noseColor; }
    }

    float noseLength = 3f;
    public override float NoseLength
    {
        get { return noseLength; }
    }

    float scale = 5f;
    public override float Scale
    {
        get { return scale; }
        set
        {
            if (value < 1f)
            {
                value = 1f;
            }
            else if (value > 8f)
            {
                value = 8f;
            }
            scale = value;
        }
    }

    public override GameObject GetEars()
    {
        throw new System.NotImplementedException();
    }

    public override void Sleep()
    {
        Debug.Log("ZZZZZZZ!");
    }

    public override string Talk()
    {
        return "T�r����!";
    }
}
