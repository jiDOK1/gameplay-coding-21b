public class DwarfElephant : Elephant
{
    float scale = 1f;
    public override float Scale
    {
        get { return scale; }
        set { scale = value; }
    }

    public override string Talk()
    {
        return base.Talk() + "\nIch bin sehr klein!";
    }
}
