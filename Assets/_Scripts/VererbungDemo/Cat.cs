using UnityEngine;

public class Cat : Animal
{
    Color skinColor = Color.magenta;
    public override Color SkinColor
    {
        get { return skinColor; }
    }

    Color noseColor = new Color(0.2f, 0.7f, 0.9f, 1.0f);
    public override Color NoseColor
    {
        get { return noseColor; }
    }

    float noseLength = 1f;
    public override float NoseLength
    {
        get { return noseLength; }
    }

    float scale = 0.5f;
    public override float Scale
    {
        get { return scale; }
        set
        {
            if (value < 0.1f)
            {
                value = 0.1f;
            }
            else if (value > 1.5f)
            {
                value = 1.5f;
            }
            scale = value;
        }
    }

    public override GameObject GetEars()
    {
        throw new System.NotImplementedException();
    }

    public override void Sleep()
    {
        Debug.Log("zzzz purr zzzz");
    }

    public override string Talk()
    {
        return "Miau!";
    }
}
