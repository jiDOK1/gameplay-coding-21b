using UnityEngine;

public class AnimalTester : MonoBehaviour
{
    Animal[] animals = new Animal[4];

    void Start()
    {
        //Animal ele = new Elephant();
        //Animal cat = new Cat();
        //Debug.Log(ele.Talk());
        //ele.Sleep();
        //Debug.Log(cat.Talk());
        //cat.Sleep();

        for (int i = 0; i < animals.Length; i++)
        {
            if(i % 2 == 0)
            {
                animals[i] = new Elephant();
            }
            else
            {
                animals[i] = new Cat();
            }
        }
        for (int j = 0; j < animals.Length; j++)
        {
            Debug.Log(animals[j].Talk());
            animals[j].Sleep();
        }
    }
}
