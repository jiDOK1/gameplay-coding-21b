using UnityEngine.Events;

public class SettingsButton : UIButton
{
    public static UnityEvent Clicked = new UnityEvent();

    public override void OnClick()
    {
        Clicked?.Invoke();
    }
}
