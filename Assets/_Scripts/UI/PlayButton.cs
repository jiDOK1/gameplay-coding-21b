using UnityEngine.Events;

public class PlayButton : UIButton
{
    public static UnityEvent Clicked = new UnityEvent();

    public override void OnClick()
    {
        Clicked?.Invoke();
    }
}
