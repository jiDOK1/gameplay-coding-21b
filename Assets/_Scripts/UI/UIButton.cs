using UnityEngine;
using UnityEngine.UI;

public abstract class UIButton : MonoBehaviour
{
    void Start()
    {
        GetComponent<Button>().onClick.AddListener(OnClick);
    }

    public abstract void OnClick();
}
