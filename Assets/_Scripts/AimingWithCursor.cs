using UnityEngine;

public class AimingWithCursor : MonoBehaviour
{
    public Transform Barrel;
    public Transform ShootingStand;
    public float yOffset = 1f;
    Transform playerTransform;
    Camera cam;
    Plane bonusPlane;
    Plane levelPlane;
    Plane curPlane;

    void Awake()
    {
        playerTransform = Barrel.parent.parent.parent;
        bonusPlane = new Plane(Vector3.back, ShootingStand.position);
        levelPlane = new Plane(Vector3.up, Vector3.zero);
        curPlane = bonusPlane;
        cam = Camera.main;
    }

    void Update()
    {
        Ray ray = cam.ScreenPointToRay(Input.mousePosition);
        float distance;
        curPlane.Raycast(ray, out distance);
        //Debug.DrawRay(ray.origin, ray.direction * distance, Color.red);
        Vector3 aimPos = ray.origin + ray.direction * distance;
        if (Vector3.SqrMagnitude(aimPos - playerTransform.position) < 1f)
        {
            return;
        }
        aimPos += Vector3.up * yOffset;
        // TODO: statt direkt draufzuschauen, hinmorphen
        Barrel.LookAt(aimPos);
    }

    public void SetPlane(PlayerState state)
    {
        switch (state)
        {
            case PlayerState.Bonus:
                curPlane = bonusPlane;
                break;
            case PlayerState.Off:
                break;
            case PlayerState.Level:
                curPlane = levelPlane;
                break;
            default:
                break;
        }
    }

    public void SetWeapon(Weapon w)
    {
        Barrel = w.transform.GetChild(0);
    }
}
