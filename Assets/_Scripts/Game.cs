using System.Collections;
using UnityEngine;

public class Game : MonoBehaviour
{
    [SerializeField] Transform camPoint01;
    [SerializeField] float bonus1Duration = 20f;
    [SerializeField] Animator stand1Anim;
    [SerializeField] CanSpawner canSpawner;
    [SerializeField] EnemySpawner enemySpawner;
    [SerializeField] GameObject StartPanel;
    [SerializeField] GameObject SettingsPanel;
    [SerializeField] Texture2D Reticle;
    float b1tol1Duration;
    float timer;
    Player player;
    WaitForSeconds bonus1wfs;
    Camera cam;
    Vector3 playerToPos;
    GameState lastState = GameState.Bonus1;
    bool isStarting = true;

    void Awake()
    {
        cam = Camera.main;
        player = GameObject.Find("Player").GetComponent<Player>();
        playerToPos = player.transform.position;
        bonus1wfs = new WaitForSeconds(bonus1Duration);
        Cursor.lockState = CursorLockMode.Confined;
    }

    void Start()
    {
        PlayButton.Clicked.AddListener(Play);
        SettingsButton.Clicked.AddListener(Settings);
        SwitchState(GameState.Pause);
        AnimationClip[] clips = stand1Anim.runtimeAnimatorController.animationClips;
        b1tol1Duration = clips[0].length;
    }

    void Update()
    {
        if (lastState == GameState.Pause) { return; }
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            SwitchState(GameState.Pause);
        }
    }

    void SetCursor(bool normal)
    {
        if (normal)
        {
            Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);
        }
        else
        {
            Cursor.SetCursor(
                Reticle,
                new Vector2(Reticle.width / 2f, Reticle.height / 2f),
                CursorMode.Auto);
        }
    }

    void Play()
    {
        //Debug.Log("Play Button was clicked!");
        // StartPanel ausschalten
        StartPanel.SetActive(false);
        // SettingsPanel ausschalten
        SettingsPanel.SetActive(false);
        // Set Reticle Cursor
        SetCursor(false);
        // Unpause
        Time.timeScale = 1f;
        if (isStarting)
        {
            SwitchState(GameState.Bonus1);
            isStarting = false;
        }
        else
        {
            ReturnToState(lastState);
        }
    }

    void Settings()
    {
        //Debug.Log("Settings Button was clicked!");
        // StartPanel ausschalten
        // Settings Panel anschalten
        SettingsPanel.SetActive(true);
        // Pause
    }

    void SwitchState(GameState s)
    {
        switch (s)
        {
            case GameState.Pause:
                // pause
                SetCursor(true);
                player.SwitchState(PlayerState.Off);
                if (!isStarting)
                {
                    Settings();
                }
                Time.timeScale = 0f;
                break;
            case GameState.Bonus1:
                lastState = GameState.Bonus1;
                player.SwitchState(PlayerState.Bonus);
                StartCoroutine(Bonus1TimerCO());
                break;
            case GameState.B1ToL1:
                lastState = GameState.B1ToL1;
                player.SwitchState(PlayerState.Off);
                stand1Anim.transform.Find("CanTrigger").GetComponent<BoxCollider>().enabled = false;
                stand1Anim.SetTrigger("godown");
                StartCoroutine(BL1ToL1TimerCO());
                break;
            case GameState.Level1:
                lastState = GameState.Level1;
                canSpawner.DestroyCans();
                enemySpawner.StartSpawing(8);
                player.SwitchState(PlayerState.Level);
                break;
            default:
                Debug.LogError("state not defined");
                break;
        }
    }

    void ReturnToState(GameState s)
    {
        switch (s)
        {
            case GameState.Pause:
                break;
            case GameState.Bonus1:
                SetCursor(false);
                player.SwitchState(PlayerState.Bonus);
                break;
            case GameState.B1ToL1:
                SetCursor(false);
                break;
            case GameState.Level1:
                player.SwitchState(PlayerState.Level);
                SetCursor(false);
                break;
            default:
                Debug.LogError("state not defined");
                break;
        }
    }

    IEnumerator Bonus1TimerCO()
    {
        yield return bonus1wfs;
        SwitchState(GameState.B1ToL1);
    }

    IEnumerator BL1ToL1TimerCO()
    {
        timer = 0f;
        Vector3 playerFromPos = player.transform.position;
        Vector3 fromPos = cam.transform.position;
        Vector3 toPos = camPoint01.position;
        Quaternion fromRot = cam.transform.rotation;
        Quaternion toRot = camPoint01.rotation;
        while (timer < b1tol1Duration)
        {
            timer += Time.deltaTime;
            float t = timer / b1tol1Duration;
            player.transform.position = Vector3.Lerp(playerFromPos, playerToPos, t);
            cam.transform.position = Vector3.Lerp(fromPos, toPos, t);
            cam.transform.rotation = Quaternion.Lerp(fromRot, toRot, t);
            yield return null;
        }
        cam.transform.parent = player.transform;
        SwitchState(GameState.Level1);
    }
}

public enum GameState
{
    Pause, Bonus1, B1ToL1, Level1
}
