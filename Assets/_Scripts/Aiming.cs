using UnityEngine;

public class Aiming : MonoBehaviour
{
    public Transform Turret;
    public Transform Barrel;
    public float rotSpeed = 10f;
    float xRot;
    float yRot;

    void Update()
    {
        xRot -= Input.GetAxis("Mouse Y") * rotSpeed;
        yRot += Input.GetAxis("Mouse X") * rotSpeed;
        xRot = Mathf.Clamp(xRot, -50f, 15f);
        yRot = Mathf.Clamp(yRot, -65f, 65f);
        Barrel.localEulerAngles = new Vector3(xRot, Barrel.localEulerAngles.y, Barrel.localEulerAngles.z);
        Turret.localEulerAngles = new Vector3(Turret.localEulerAngles.x, yRot, Turret.localEulerAngles.z);
    }
}
