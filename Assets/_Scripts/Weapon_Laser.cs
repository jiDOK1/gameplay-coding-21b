using UnityEngine;

public class Weapon_Laser : Weapon
{
    public override void Fire()
    {
        Ray ray = new Ray(SpawnPoint.position, SpawnPoint.forward);
        RaycastHit hitInfo;
        Debug.DrawRay(SpawnPoint.position, SpawnPoint.forward * ShootPower, Color.red, 1f);
        if (Physics.Raycast(ray, out hitInfo, ShootPower))
        {
            Enemy enemy = hitInfo.transform.GetComponent<Enemy>();
            if (enemy != null)
            {
                enemy.TakeDamage(Damage);
            }
        }
    }

    public override void EndFire()
    {
    }
}
