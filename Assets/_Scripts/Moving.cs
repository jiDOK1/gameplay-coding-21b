using UnityEngine;

public abstract class Moving : MonoBehaviour
{
    public float Speed = 5f;
    protected CharacterController cc;
    protected Vector3 gravity = Vector3.down * 9.81f;

    public abstract void OnUpdate();

    public virtual void Awake()
    {
        cc = GetComponent<CharacterController>();
    }
}
