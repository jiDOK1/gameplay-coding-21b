using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class DelegatesEvents : MonoBehaviour
{
    public int Health = 15;
    public string Name = "Annika";
    //public event Action<string> OnChristmas;
    public delegate void MyDelegate();
    MyDelegate myDelegate;
    StringEvent message = new StringEvent();

    public event Action<DelegatesEvents> SomeEvent;



    void Start()
    {
        myDelegate = Test;
        myDelegate += Test2;
        message?.Invoke("hello");
        SomeEvent?.Invoke(this);
        //OnChristmas += SingChristmasSong;
        //OnChristmas += SnowFall;
        //OnChristmas?.Invoke("Merry Christmas");
    }

    void PrintHealth(DelegatesEvents del)
    {
        print(del.Name + " has Health: ");
        print(del.Health);
    }

    void Test()
    {
        print("testing");
    }

    void Test2()
    {
        print("still testing");
    }

    void TestTheTest(MyDelegate d)
    {
        d();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            TestTheTest(myDelegate);
        }
        //if (Input.GetButtonDown("Jump"))
        //{
        //    OnChristmas?.Invoke("It is still Christmas");
        //}
    }
    //void SingChristmasSong(string msg)
    //{
    //    print("Stille Nacht");
    //    print(msg);
    //}

    //void SnowFall(string msg)
    //{
    //    print("Es schneit");
    //    OnChristmas -= SnowFall;
    //    print(msg);
    //}

}

public class StringEvent : UnityEvent<string>{}
