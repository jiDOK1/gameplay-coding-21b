using UnityEngine;
using UnityEngine.UI;

public class UIDemo : MonoBehaviour
{
    public Button Button;

    void Start()
    {
        Button.onClick.AddListener(EndGame);
    }

    void Update()
    {
        if (Input.GetButtonDown("Jump"))
        {
        }
    }

    public void EndGame()
    {
        //Debug.Log("Game Over!");
        Application.Quit();
    }
}
