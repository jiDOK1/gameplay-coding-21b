using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    public Enemy Enemy;
    public float SpawnDelay = 1f;
    bool isSpawning;
    float spawnTimer;
    int count;
    Transform[] spawnPoints;

    void Awake()
    {
        spawnTimer = SpawnDelay;
        spawnPoints = new Transform[transform.childCount];
        for (int i = 0; i < spawnPoints.Length; i++)
        {
            spawnPoints[i] = transform.GetChild(i);
        }
    }

    void Update()
    {
        if (!isSpawning) { return; }
        spawnTimer += Time.deltaTime;
        if (spawnTimer >= SpawnDelay)
        {
            spawnTimer = 0;
            Spawn();
            count--;
            if(count <= 0)
            {
                isSpawning = false;
            }
        }
    }

    public void StartSpawing(int countPerSpawnpoint)
    {
        count = countPerSpawnpoint;
        isSpawning = true;
    }

    void Spawn()
    {
        for (int i = 0; i < spawnPoints.Length; i++)
        {
            Instantiate<Enemy>(Enemy, spawnPoints[i].position, Quaternion.identity);
        }
    }
}
