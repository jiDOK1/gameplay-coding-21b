using UnityEngine;

public class CanTrigger : MonoBehaviour
{
    Player player;

    void Awake()
    {
        player = GameObject.Find("Player").GetComponent<Player>();
    }

    void OnCollisionEnter(Collision col)
    {
        Can can = col.collider.GetComponent<Can>();
        if (can != null)
        {
            player.AddBonusPoints(can.GetBonusPoints());
            Destroy(can);
        }
    }
}
