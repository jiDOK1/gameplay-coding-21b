using UnityEngine;

public class FreeMoving : Moving
{
    [SerializeField] float rotSpeed = 2f;
    Transform baseMesh;
    //Quaternion tRot;

    public override void Awake()
    {
        base.Awake();
        baseMesh = transform.Find("MovingTurret");
    }

    public override void OnUpdate()
    {
        float xVelocity = Input.GetAxis("Horizontal");
        float zVelocity = Input.GetAxis("Vertical");
        Vector3 velocity = new Vector3(xVelocity, 0f, zVelocity);
        velocity.Normalize();
        if (velocity.sqrMagnitude > 0f)
        {
            // Rotation
            float step = rotSpeed * Time.deltaTime;
            Quaternion targetRot = Quaternion.LookRotation(velocity) * Quaternion.Euler(0f, -90f, 0f);
            //tRot = targetRot;
            baseMesh.rotation = Quaternion.RotateTowards(baseMesh.rotation, targetRot, step);
        }
        velocity += gravity;
        //Debug.DrawRay(transform.position, velocity * 10f, Color.red);
        velocity *= Speed * Time.deltaTime;
        cc.Move(velocity);
    }

    //private void OnGUI()
    //{
    //    GUILayout.Label(tRot.eulerAngles.ToString());
    //}
}
