using UnityEngine;

public class LRMoving : Moving
{
    public override void OnUpdate()
    {
        float xVelocity = Input.GetAxis("Horizontal") * Speed * Time.deltaTime;
        Vector3 velocity = new Vector3(xVelocity, 0f, 0f);
        velocity += gravity;
        cc.Move(velocity);
        // Clamp x
        if (transform.position.x < -7f)
        {
            transform.position = new Vector3(-7f, transform.position.y, transform.position.z);
        }
        else if (transform.position.x > 7f)
        {
            transform.position = new Vector3(7f, transform.position.y, transform.position.z);
        }
    }
}
