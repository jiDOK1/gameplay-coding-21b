using UnityEngine;

public class DummyEnemy : Enemy
{
    public GUIStyle style;
    public float MoveSpeed = 15f;
    public float Health = 100f;
    Transform playerTransform;

    public override void TakeDamage(float damage)
    {
        Health -= damage;
        if (Health <= 0f)
        {
            Destroy(gameObject);
        }
    }

    void Start()
    {
        playerTransform = GameObject.Find("Player").transform;
    }

    void Update()
    {
        Vector3 dir = playerTransform.position - transform.position;
        transform.Translate(dir.normalized * Time.deltaTime * MoveSpeed);
    }

    private void OnTriggerEnter(Collider other)
    {
        Player player = other.gameObject.GetComponent<Player>();
        //Debug.Log($"Found Player: {player != null}");
        if (player != null)
        {
            player.TakeDamage(10);
            Destroy(gameObject);
        }
    }

    //void OnGUI()
    //{
    //    GUILayout.Label(Health.ToString(), style);
    //}
}
