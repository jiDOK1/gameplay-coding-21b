using UnityEngine;

public class Can : MonoBehaviour
{
    [SerializeField] int BonusPoints;
    [SerializeField] AudioClip[] clips;
    [SerializeField] AudioClip softClip;
    [SerializeField] float colStrengthThreshold;
    float soundTimer;
    bool mayPlaySound;

    void Update()
    {
        if (soundTimer < 0.5f)
        {
            soundTimer += Time.deltaTime;
            mayPlaySound = true;
        }
    }

    public int GetBonusPoints()
    {
        return BonusPoints;
    }

    void OnCollisionEnter(Collision collision)
    {
        if (!mayPlaySound) { return; }
        if (collision.relativeVelocity.magnitude > colStrengthThreshold)
        {
            int randIdx = Random.Range(0, clips.Length);
            AudioSource.PlayClipAtPoint(clips[randIdx], transform.position);
        }
        else
        {
            AudioSource.PlayClipAtPoint(softClip, transform.position, 0.2f);
        }
    }
}
