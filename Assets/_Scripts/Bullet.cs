using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float Damage = 25f;
    [SerializeField] float bulletLife = 10f;
    float timer;

    void Update()
    {
        timer += Time.deltaTime;
        if(timer >= bulletLife)
        {
            Destroy(gameObject);
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        Enemy enemy = collision.gameObject.GetComponent<Enemy>();
        if (enemy != null)
        {
            enemy.TakeDamage(Damage);
        }
        Destroy(gameObject);
    }
}
