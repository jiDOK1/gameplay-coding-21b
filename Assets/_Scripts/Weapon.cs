using UnityEngine;

public abstract class Weapon : MonoBehaviour
{
    public Transform SpawnPoint;
    public float ShootPower;
    public float Damage;
    public abstract void Fire();
    public abstract void EndFire();
}
