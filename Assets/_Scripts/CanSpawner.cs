using UnityEngine;

public class CanSpawner : MonoBehaviour
{
    public GameObject[] CanPrefabs;
    Transform[] spawnPoints;
    GameObject[] cans;

    void Start()
    {
        //spawnPoints = GetComponentsInChildren<Transform>();
        cans = new GameObject[transform.childCount];
        spawnPoints = new Transform[transform.childCount];
        for (int i = 0; i < spawnPoints.Length; i++)
        {
            spawnPoints[i] = transform.GetChild(i);
        }
        SpawnCans();
    }

    public void SpawnCans()
    {
        for (int i = 0; i < spawnPoints.Length; i++)
        {
            GameObject prefab = CanPrefabs[Random.Range(0, CanPrefabs.Length)];
            GameObject can = Instantiate<GameObject>(prefab, spawnPoints[i].position, Quaternion.identity);
            can.transform.parent = transform;
            cans[i] = can;
        }
    }

    public void DestroyCans()
    {
        for (int i = cans.Length - 1; i >= 0; i--)
        {
            Destroy(cans[i]);
        }
        //cans = null;
    }
}
