using UnityEngine;

public class WeaponPickup : MonoBehaviour
{
    public Weapon WeaponPrefab;

    void OnTriggerEnter(Collider other)
    {
        Weapon weapon = other.GetComponentInChildren<Weapon>();
        if (weapon == null) { return; }
        Destroy(weapon.gameObject);
        Destroy(gameObject);
        Transform weaponSlot = other.transform.Find("WeaponSlot");
        Weapon newWeapon = Instantiate<Weapon>(
            WeaponPrefab,
            weaponSlot.position,
            weaponSlot.rotation,
            weaponSlot);

        Shooting shooting = other.GetComponent<Shooting>();
        shooting.SetWeapon(newWeapon);
        AimingWithCursor aiming = other.GetComponent<AimingWithCursor>();
        aiming.SetWeapon(newWeapon);
    }
}
