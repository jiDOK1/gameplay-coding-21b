using UnityEngine;

public class Weapon_Cannon : Weapon
{
    public GameObject BulletPrefab;

    public override void Fire()
    {
        GameObject bullet = Instantiate<GameObject>(BulletPrefab, SpawnPoint.position, Quaternion.identity);
        Vector3 shootDir = SpawnPoint.forward;
        bullet.GetComponent<Rigidbody>().AddForce(shootDir * ShootPower, ForceMode.Impulse);
    }

    public override void EndFire()
    {
    }
}
