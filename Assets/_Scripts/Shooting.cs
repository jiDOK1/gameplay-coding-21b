using UnityEngine;

public class Shooting : MonoBehaviour
{
    Weapon weapon;

    void Awake()
    {
        weapon = GetComponentInChildren<Weapon>();
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            weapon?.Fire();
        }
        if (Input.GetMouseButtonUp(0))
        {
            weapon?.EndFire();
        }
    }

    public void SetWeapon(Weapon w)
    {
        weapon = w;
    }
}
